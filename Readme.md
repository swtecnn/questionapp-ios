# Setup

1. Install Xcode
2. [Install Cocoapods](https://guides.cocoapods.org/using/getting-started.html):
    ```gem install cocoapods```

# How to build

1. clone project
2. ```cd questionapp-ios/QuestionsApp```
3. ```pod install```
4. Open the QuestionsApp.xcworkspace and start building
