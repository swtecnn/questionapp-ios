// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

public protocol RaiseHandRepositoryListener {
    
    func requestCountChanged(socialEventId: String, count: Int)
}
