// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation
import Firebase
import FirebaseMessaging

class FirebaseMessagingService: NSObject, IMessagingService, ITokenProvider {
    
    lazy var functions = Functions.functions()
    
    var token: String?
    
    override init() {
        super.init()
        Messaging.messaging().delegate = self
        registerForPushNotifications()
    }
    
    func send(message: Message) throws {

        functions.httpsCallable("sendQuestion").call(message.asPayload()) { (result, error) in
            if let error = error as NSError? {
                if error.domain == FunctionsErrorDomain {
                    let code = FunctionsErrorCode(rawValue: error.code)
                    let message = error.localizedDescription
                    let details = error.userInfo[FunctionsErrorDetailsKey]
                    
                    print("Error \(String(describing: code)) \(message) \(String(describing: details))")
                }
            }
            if let text = (result?.data as? [String: Any])?["text"] as? String {
                print(text)
            }
        }
    }
    
    private func registerForPushNotifications() {
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.delegate = self
        
        let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
        notificationCenter.requestAuthorization(options: authOptions) { granted, error in
            guard granted else { return }
            asyncMain {
                UIApplication.shared.registerForRemoteNotifications()
            }
        }
    }
}

extension FirebaseMessagingService: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print(remoteMessage.appData)
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        token = fcmToken
    }
}

extension FirebaseMessagingService: UNUserNotificationCenterDelegate {

}

extension Message {
    func asPayload() -> [String: String] {
        return [
            "token": token,
            "message": message,
            "title": title
        ]
    }
}

