// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

public struct RaiseHandRequestStatus: Decodable {
    
    public enum State: String, Decodable {
        case inQueue = "in queue"
        case archived = "archived"
        case active = "active"
        case canceled = "canceled"
    }
    
    public let order: Int
    public let state: State
}
