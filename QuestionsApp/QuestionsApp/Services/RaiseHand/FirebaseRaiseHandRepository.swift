// Copyright © 2019 SWTecNN. All rights reserved.

import Firebase

class FirebaseRaiseHandRepository: RaiseHandRepository {

    private let timeout = TimeInterval(30)

    private let databaseReference: DatabaseReference

    init(reference: DatabaseReference) {

        databaseReference = reference
    }

    func addRaiseHandRequest(_ request: RaiseHandRequest) throws -> String {

        var result: String?
        let semaphore = BinarySemaphore()
        var err: RaiseHandRepositoryError?

        databaseReference.child(FirebasePaths.raiseHandRequests).childByAutoId().setValue(request.asFirebaseModel()) { (error, reference) in

            if error != nil  {
                err = .unableToAdd
            } else if let id = reference.key {
                result = id
            }

            semaphore.signal()
        }

        if semaphore.wait(timeout: timeout) {
            if let result = result {
                return result
            } else {
                throw err ?? .unableToAdd
            }
        } else {
            throw RaiseHandRepositoryError.timeout
        }
    }

    func deleteRaiseHandRequest(id: String) throws {

        let semaphore = BinarySemaphore()
        var err: RaiseHandRepositoryError?

        databaseReference.child(FirebasePaths.raiseHandRequests).child(id).removeValue { (error, reference) in

            if error != nil {
                err = .unableToRemove
            }
            semaphore.signal()
        }

        if !semaphore.wait(timeout: timeout) {
            err = .timeout
        }

        if let error = err {
            throw error
        }
    }

    func getRaiseHandRequests(for socialId: String) throws -> [RaiseHandRequest] {

        var result: [RaiseHandRequest]?
        let semaphore = BinarySemaphore()
        var err: RaiseHandRepositoryError?

        databaseReference.child(FirebasePaths.raiseHandRequests).observeSingleEvent(of: .value, with: { (snapshot) in

            if let requests = RaiseHandRequest.createArray(from: snapshot) {
                result = requests.filter({ $0.socialEventId == socialId })
            } else {
                result = []
            }

            semaphore.signal()
        }) { (error) in
            err = .unableToGet
        }

        if semaphore.wait(timeout: timeout) {
            if let result = result {
                return result
            } else {
                throw err ?? .unableToGet
            }
        } else {
            throw QuestionRepositoryError.timeout
        }
    }

    func addListener(_ listener: RaiseHandRepositoryListener) {

        databaseReference.child(FirebasePaths.raiseHandRequests).observe(.value, with: { snapshot in

            guard let requests = RaiseHandRequest.createArray(from: snapshot) else {
                return
            }

            var dictionary = [String: Int]()
            for request in requests {
                dictionary[request.socialEventId] = (dictionary[request.socialEventId] ?? 0) + 1
            }

            for key in dictionary.keys {
                listener.requestCountChanged(socialEventId: key, count: dictionary[key] ?? 0)
            }
        })
    }
}
