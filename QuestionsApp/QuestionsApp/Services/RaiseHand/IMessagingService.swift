// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

public struct Message {
    let message: String
    let title: String
    let token: String
}

public protocol IMessagingService {
    func send(message: Message) throws
}
