// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

enum RaiseHandRepositoryError: Error {

    case timeout
    case unableToAdd
    case unableToRemove
    case unableToGet
    case empty
}

public protocol RaiseHandRepository {
    func addRaiseHandRequest(_ request: RaiseHandRequest) throws -> String
    func deleteRaiseHandRequest(id: String) throws
    func getRaiseHandRequests(for socialId: String) throws -> [RaiseHandRequest]
    
    func addListener(_ listener: RaiseHandRepositoryListener)
}
