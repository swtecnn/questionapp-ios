// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation
import Firebase

public struct RaiseHandRequest: Codable {
    let id: String?
    let socialEventId: String
    let pushNotificationToken: String
    let name: String
    let timestamp: Date
}

extension RaiseHandRequest {

    func asFirebaseModel() -> [String: Any] {

        let encoder = JSONEncoder()
        encoder.dateEncodingStrategy = .millisecondsSince1970
        guard let data = try? encoder.encode(self),
            let dictionary = try? JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any] else {
            return [:]
        }

        return dictionary
    }

    static func create(from object: Any, with id: String) -> RaiseHandRequest? {

        guard var dictionary = object as? [String: Any] else {
            return nil
        }

        dictionary[CodingKeys.id.stringValue] = id

        guard let data = try? JSONSerialization.data(withJSONObject: dictionary, options: .prettyPrinted) else {
            return nil
        }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .millisecondsSince1970
        return try? decoder.decode(RaiseHandRequest.self, from: data)
    }

    static func createArray(from snapshot: DataSnapshot) -> [RaiseHandRequest]? {

        guard let snapshotDict = snapshot.value as? [String: Any] else {
            return nil
        }

        return snapshotDict.compactMap({ RaiseHandRequest.create(from: $1, with: $0) })
    }
}
