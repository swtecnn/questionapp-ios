// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

public protocol ITokenProvider {
    var token: String? { get }
}
