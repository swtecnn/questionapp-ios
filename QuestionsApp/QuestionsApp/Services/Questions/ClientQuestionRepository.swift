// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

public protocol ClientQuestionRepository {
    
    func addQuestion(id: String) throws
    func deleteQuestion(with id: String) throws
    func getQuestions() throws -> [String]

    func updateVote(for questionId: String) throws
    func getVotedQuestions() throws -> [String]
}
