// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

public protocol QuestionRepositoryListener {
    
    func questionsCountChanged(socialEventId: String, count: Int)
}
