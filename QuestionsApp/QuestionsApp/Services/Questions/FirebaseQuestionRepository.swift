// Copyright © 2019 SWTecNN. All rights reserved.

import Firebase

class FirebaseQuestionRepository: QuestionRepository {

    private let timeout = TimeInterval(30)

    private let databaseReference: DatabaseReference

    init(reference: DatabaseReference) {

        databaseReference = reference
    }

    func addQuestion(_ question: Question) throws -> String {

        var result: String?
        let semaphore = BinarySemaphore()
        var err: QuestionRepositoryError?

        databaseReference.child(FirebasePaths.questions).childByAutoId().setValue(question.asFirebaseModel()) { (error, reference) in

            if error != nil  {
                err = .unableToAdd
            } else if let id = reference.key {
                result = id
            }
            
            semaphore.signal()
        }

        if semaphore.wait(timeout: timeout) {
            if let result = result {
                return result
            } else {
                throw err ?? .unableToAdd
            }
        } else {
            throw QuestionRepositoryError.timeout
        }
    }

    func getQuestions(for socialId: String) throws -> [Question] {

        var result: [Question]?
        let semaphore = BinarySemaphore()
        var err: QuestionRepositoryError?

        databaseReference.child(FirebasePaths.questions).observeSingleEvent(of: .value, with: { (snapshot) in

            if let questions = Question.createArray(from: snapshot) {
                result = questions.filter({ $0.socialEventId == socialId })
            } else {
                err = .unableToGetQuestions
            }

            semaphore.signal()
        }) { (error) in
            err = .unableToGetQuestions
        }

        if semaphore.wait(timeout: timeout) {
            if let result = result {
                return result
            } else {
                throw err ?? .unableToGetQuestions
            }
        } else {
            throw QuestionRepositoryError.timeout
        }
    }

    func updateVoteForQuestion(with questionId: String, vote: Int) throws {

        let semaphore = BinarySemaphore()
        var err: QuestionRepositoryError?

        databaseReference.child(FirebasePaths.questions).child(questionId).runTransactionBlock({ (mutableData) -> TransactionResult in

            if var question = Question.create(from: mutableData.value as Any, with: questionId) {

                question.votes += vote
                mutableData.value = question.asFirebaseModel()
            }

            return TransactionResult.success(withValue: mutableData)
        }) { (error, commited, snapshot) in

            if error != nil {
                err = .unableToUpdateVote
            }

            semaphore.signal()
        }

        if !semaphore.wait(timeout: timeout) {
            err = .timeout
        }

        if let error = err {
            throw error
        }
    }

    func deleteQuestion(with questionId: String) throws {

        let semaphore = BinarySemaphore()
        var err: QuestionRepositoryError?

        databaseReference.child(FirebasePaths.questions).child(questionId).removeValue { (error, reference) in

            if error != nil {
                err = .unableToRemove
            }
            semaphore.signal()
        }

        if !semaphore.wait(timeout: timeout) {
            err = .timeout
        }

        if let error = err {
            throw error
        }
    }

    func archiveQuestion(with questionId: String) throws {

        let semaphore = BinarySemaphore()
        var err: QuestionRepositoryError?

        databaseReference.child(FirebasePaths.questions).child(questionId).updateChildValues([Question.archivedKey: true]) { (error, reference) in

            if error != nil {
                err = .unableToArchive
            }

            semaphore.signal()
        }

        if !semaphore.wait(timeout: timeout) {
            err = .timeout
        }

        if let error = err {
            throw error
        }
    }

    func addListener(_ listener: QuestionRepositoryListener) {

        databaseReference.child(FirebasePaths.questions).observe(.value, with: { snapshot in

            guard let questions = Question.createArray(from: snapshot) else {
                return
            }

            var dictionary = [String: Int]()
            for question in questions {
                if !question.archived {
                    dictionary[question.socialEventId] = (dictionary[question.socialEventId] ?? 0) + 1
                }
            }

            for key in dictionary.keys {
                listener.questionsCountChanged(socialEventId: key, count: dictionary[key] ?? 0)
            }
        })
    }
}
