// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

public enum QuestionRepositoryError: Error {

    case timeout
    case unableToAdd
    case unableToGetQuestions
    case unableToArchive
    case unableToRemove
    case unableToUpdateVote
}

public protocol QuestionRepository {
    
    func addQuestion(_ question: Question) throws -> String
    func getQuestions(for socialId: String) throws -> [Question]
    func updateVoteForQuestion(with questionId: String, vote: Int) throws
    func deleteQuestion(with questionId: String) throws
    func archiveQuestion(with questionId: String) throws
    
    func addListener(_ listener: QuestionRepositoryListener)
}
