// Copyright © 2019 SWTecNN. All rights reserved.

struct FirebasePaths {

    static let socialEvents = "social_events"
    static let questions = "questions"
    static let raiseHandRequests = "raise_hand_requests"
}
