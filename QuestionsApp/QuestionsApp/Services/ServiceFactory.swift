// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation
import Firebase
import AppCenter
import AppCenterAnalytics
import AppCenterCrashes

open class Service {
    
    public static var factory: IServicesFactory!
    
}

open class ServicesFactory: IServicesFactory {
    
    private let analyticsRegistry: AnalyticsEventRegistry
    private let deviceInfoProvider: DeviceInfoProvider
    private let socialEventRepository: SocialEventRepository
    private let questionRepository: QuestionRepository
    private let raiseHandRepository: RaiseHandRepository
    private let clientStorage: ClientStorage
    private let socialEventCipher: ISocialEventCipher
    private let messagingService: IMessagingService

    public init() {
        
        if FirebaseApp.app() == nil {
            FirebaseApp.configure()
        }
        
        let messaging = FirebaseMessagingService()
        messagingService = messaging
        deviceInfoProvider = DefaultDeviceInfoProvider(tokenProvider: messaging)
        analyticsRegistry = FlurryAnalyticsEventsRegistry(key: "", deviceInfo: deviceInfoProvider)

        let databaseReference = Database.database().reference()
        socialEventRepository = FirebaseSocialEventRepository(reference: databaseReference)
        questionRepository = FirebaseQuestionRepository(reference: databaseReference)
        raiseHandRepository = FirebaseRaiseHandRepository(reference: databaseReference)

        let keyValueStorage = UserDefaultsStorage()
        clientStorage = ClientStorage(storage: keyValueStorage)

        socialEventCipher = SocialEventCipher()
        
        setupAnalitycs()
    }
    
    public func getAnalyticsRegistry() -> AnalyticsEventRegistry {
        return analyticsRegistry
    }
    
    public func getDeviceInfoProvider() -> DeviceInfoProvider {
        return deviceInfoProvider
    }
    
    public func getSocialEventRepository() -> SocialEventRepository {
        return socialEventRepository
    }
    
    public func getQuestionRepository() -> QuestionRepository {
        return questionRepository
    }
    
    public func getRaiseHandRepository() -> RaiseHandRepository {
        return raiseHandRepository
    }

    public func getClientQuestionRepository() -> ClientQuestionRepository {
        return clientStorage
    }

    public func getClientSessionRepository() -> ClientSessionRepository {
        return clientStorage
    }

    public func getClientSocialEventRepository() -> ClientSocialEventRepository {
        return clientStorage
    }

    public func getSocialEventCipher() -> ISocialEventCipher {
        return socialEventCipher
    }
    
    public func getMessagingService() -> IMessagingService {
        return messagingService
    }
    
    private func setupAnalitycs() {
        
        MSAppCenter.start("de0bfa78-fcf5-4427-8e9b-4f6640577236", withServices:[
          MSAnalytics.self,
          MSCrashes.self
        ])
    }
}
