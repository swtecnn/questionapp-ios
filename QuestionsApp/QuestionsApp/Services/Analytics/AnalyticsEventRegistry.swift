// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public protocol AnalyticsEventRegistry {
    
    func register(event: AnalyticsEvent)
}
