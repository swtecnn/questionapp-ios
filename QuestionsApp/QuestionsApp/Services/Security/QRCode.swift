// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

class QRCode: Decodable {

    let socialEventId: String
    let passwordHash: String?
}

extension QRCode {

    static func create(from base64: String) -> QRCode? {

        guard let data = Data(base64Encoded: base64) else {
            return nil
        }

        return try? JSONDecoder().decode(QRCode.self, from: data)
    }
}
