// Copyright © 2019 SWTecNN. All rights reserved.

public protocol ISocialEventCipher {

    func encrypt(code: String) -> (code: String, hash: String)
    func decrypt(code: String, hash: String) -> String
}
