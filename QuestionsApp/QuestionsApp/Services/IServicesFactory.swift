// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public protocol IServicesFactory {
    
    func getAnalyticsRegistry() -> AnalyticsEventRegistry
    func getDeviceInfoProvider() -> DeviceInfoProvider
    func getSocialEventRepository() -> SocialEventRepository
    func getQuestionRepository() -> QuestionRepository
    func getRaiseHandRepository() -> RaiseHandRepository
    func getClientQuestionRepository() -> ClientQuestionRepository
    func getClientSessionRepository() -> ClientSessionRepository
    func getClientSocialEventRepository() -> ClientSocialEventRepository
    func getSocialEventCipher() -> ISocialEventCipher
    func getMessagingService() -> IMessagingService
}
