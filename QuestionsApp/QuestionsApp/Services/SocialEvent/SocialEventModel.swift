// Copyright © 2019 SWTecNN. All rights reserved.

public struct SocialEventModel {

    let socialEvent: SocialEvent
    let role: ClientSocialEventRole
}
