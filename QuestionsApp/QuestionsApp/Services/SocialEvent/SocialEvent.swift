// Copyright © 2019 SWTecNN. All rights reserved.

import Firebase

public struct SocialEvent: Codable {

    let id: String
    let title: String
    let description: String
    let password: String
    let date: Date
    let archived: Bool
    let lector: String
    let agenda: String
    let usefulLinks: [String]
}

extension SocialEvent {

    static func create(from snapshot: DataSnapshot, with id: String) -> SocialEvent? {

        guard var snapshotDict = snapshot.value as? [String: Any] else {
            return nil
        }

        snapshotDict[CodingKeys.id.stringValue] = id

        guard let data = try? JSONSerialization.data(withJSONObject: snapshotDict, options: .prettyPrinted) else {
            return nil
        }

        let decoder = JSONDecoder()
        decoder.dateDecodingStrategy = .millisecondsSince1970
        return try? decoder.decode(SocialEvent.self, from: data)
    }
}

extension SocialEvent {

    static var archivedKey: String {

        return CodingKeys.archived.stringValue
    }
}
