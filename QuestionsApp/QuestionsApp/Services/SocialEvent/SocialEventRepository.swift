// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

public enum SocialEventRepositoryError: Error {

    case timeout
    case noSocialEventForId
    case unableToArchive
}

public protocol SocialEventRepository {
    func getSocialEvent(with id: String) throws -> SocialEvent
    func archiveSocialEvent(with id: String) throws
}
