// Copyright © 2019 SWTecNN. All rights reserved.

public protocol ClientSocialEventRepository {

    func getArchivedSocialEvents() throws -> [ClientSocialEvent]
    func archive(socialEvent: ClientSocialEvent) throws

    func addRaiseHandRequest(with id: String, for socialEventId: String) throws
    func getRaiseHandRequestId(for socialEventId: String) throws -> String?
    func deleteRaiseHandRequest(for socialEventId: String) throws
}
