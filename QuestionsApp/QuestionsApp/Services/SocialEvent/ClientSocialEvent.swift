// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

enum ClientSocialEventRole: String, Codable {

    case lector = "role_lector"
    case listener = "role_listener"
}

public struct ClientSocialEvent: Codable {

    let id: String
    let role: ClientSocialEventRole
}

extension ClientSocialEvent {

    func asJsonString() -> String? {

        guard let data = try? JSONEncoder().encode(self) else {
            return nil
        }

        return String(data: data, encoding: .utf8)
    }

    static func create(from jsonString: String) -> ClientSocialEvent? {

        guard let data = jsonString.data(using: .utf8) else {
            return nil
        }

        return try? JSONDecoder().decode(ClientSocialEvent.self, from: data)
    }
}
