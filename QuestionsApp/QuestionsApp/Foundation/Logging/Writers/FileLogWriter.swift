// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

open class FileLogWriter: LogWriter {
    
    private let fileNamePrefix: String
    private let logsDirectory: URL
    private let rollingFrequency: TimeInterval
    private let maxFilesCount: Int
    
    private let writingQueue = DispatchQueue(label: "com.swtecnn.log", qos: DispatchQoS.background)
    
    public init(fileNamePrefix: String, logsDirectory: URL, rollingFrequency: TimeInterval, maxFilesCount: Int) {
        
        self.fileNamePrefix = fileNamePrefix
        self.logsDirectory = logsDirectory
        self.rollingFrequency = rollingFrequency
        self.maxFilesCount = maxFilesCount
    }
    
    open func write(message: String) {
        
        writingQueue.async {
            
            try? self.getLogFile().append(message)
            self.removeOldLogs()
        }
    }
    
    open func flush() {
        writingQueue.sync {
            //flush
        }
    }
    
    private func getLogFile() throws -> File {
        
        guard let logFile = allLogFiles().first, logFile.creationDate + rollingFrequency >= DateProvider.now() else {
            
            try FileManager.default.createDirectory(at: logsDirectory, withIntermediateDirectories: true, attributes: nil)
            return try File(folderUrl: logsDirectory, fileNamePrefix: fileNamePrefix)
        }
        
        return logFile
    }
    
    private func allLogFiles() -> [FileLogWriter.File] {
        do {
            return try FileManager.default
                .contentsOfDirectory(atPath: logsDirectory.path)
                .filter { $0.hasPrefix(self.fileNamePrefix) && $0.hasSuffix(File.extensionString) }
                .compactMap { try? File(url: logsDirectory.appendingPathComponent($0, isDirectory: false)) }
                .sorted { $0.creationDate > $1.creationDate }
        } catch {
            return []
        }
    }
    
    private func removeOldLogs() {
        
        for logFile in allLogFiles().dropFirst(maxFilesCount) {
            try? FileManager.default.removeItem(at: logFile.url)
        }
    }
    
    private class File {
        
        private static var dateFormatter = DateFormatter(format: "yyyy-MM-dd-[HH-mm]")
        
        static var extensionString = ".log"
        
        private let fileHandle: FileHandle
        
        let creationDate: Date
        let url: URL
        
        init(url: URL) throws {
            
            let fileManager = FileManager.default
            
            if !fileManager.fileExists(atPath: url.path) {
                fileManager.createFile(atPath: url.path, contents: nil, attributes: nil)
            }
            
            self.fileHandle = try FileHandle(forUpdating: url)
            self.creationDate = File.creationDate(at: url)
            self.url = url
        }
        
        convenience init(folderUrl: URL, fileNamePrefix: String) throws {
            
            let newFileName = "\(fileNamePrefix) \(File.dateFormatter.string(from: DateProvider.now()))\(File.extensionString)"
            let newFileUrl = folderUrl.appendingPathComponent(newFileName, isDirectory: false)
            try self.init(url: newFileUrl)
        }
        
        func append(_ line: String) {
            
            guard let data = "\(line)\n".data(using: .utf8) else { return }
            
            _ = fileHandle.seekToEndOfFile()
            fileHandle.write(data)
        }
        
        private static func creationDate(at url: URL) -> Date {
            
            func fromName(at url: URL) -> Date? {
                let fileName = url.deletingPathExtension().lastPathComponent
                let datePart = fileName
                    .suffix(from: fileName.firstIndex(of: " ") ?? fileName.startIndex)
                    .trimmingCharacters(in: .whitespacesAndNewlines)
                return dateFormatter.date(from: String(datePart))
            }
            
            func modificationDate(at url: URL) -> Date? {
                guard let fileAttributes = try? FileManager.default.attributesOfItem(atPath: url.path) else {
                    return nil
                }
                return fileAttributes[.modificationDate] as? Date
            }
            
            return fromName(at: url) ?? modificationDate(at: url) ?? DateProvider.now()
        }
    }
}
