// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

open class ConsoleLogWriter: LogWriter {
    
    public init() {
    }
    
    open func write(message: String) {
        print(message)
    }
    
    public func flush() {
        
    }
}
