// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public protocol LogFormatter {
    
    func format(message: String, level: Logger.Level, error: Error?, timestamp: Date, file: String, function: String, line: UInt) -> String
}
