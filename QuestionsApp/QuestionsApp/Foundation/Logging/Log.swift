// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

open class Logger {
    
    private let writer: MultipleLogWriter
    private let formatter: LogFormatter
    
    public enum Level: Int {
        case off = 0
        case error = 1
        case warning = 2
        case info = 3
        case debug = 4
        case verbose = 5
    }
    
    static var factory: LoggerFactory = DefaultLoggerFactory()
    
    public var level: Level = .error
    
    public init(formatter: LogFormatter = DefaultLogFormatter()) {
        self.writer = MultipleLogWriter(writers: [])
        self.formatter = formatter
    }
    
    @discardableResult
    open func with(writer: LogWriter) -> Logger {
        self.writer.add(writer)
        return self
    }
    
    open func flush() {
        writer.flush()
    }

    open func error(_ message: @autoclosure () -> String, error: Error? = nil, file: String = #file, function: String = #function, line: UInt = #line) {
        guard level >= .error else { return }
        write(message: message(), level: .error, error: error, file: file, function: function, line: line)
    }
    
    open func warning(_ message: @autoclosure () -> String, file: String = #file, function: String = #function, line: UInt = #line) {
        guard level >= .warning else { return }
        write(message: message(), level: .warning, error: nil, file: file, function: function, line: line)
    }
    
    open func info(_ message: @autoclosure () -> String, file: String = #file, function: String = #function, line: UInt = #line) {
        guard level >= .info else { return }
        write(message: message(), level: .info, error: nil, file: file, function: function, line: line)
    }
    
    open func debug(_ message: @autoclosure () -> String, file: String = #file, function: String = #function, line: UInt = #line) {
        guard level >= .debug else { return }
        write(message: message(), level: .debug, error: nil, file: file, function: function, line: line)
    }
    
    open func verbose(_ message: @autoclosure () -> String, file: String = #file, function: String = #function, line: UInt = #line) {
        guard level >= .verbose else { return }
        write(message: message(), level: .verbose, error: nil, file: file, function: function, line: line)
    }
    
    private func write(message: String, level: Logger.Level, error: Error?, file: String, function: String, line: UInt) {
        
        let formattedMessage = formatter.format(message: message,
                                                level: level,
                                                error: error,
                                                timestamp: DateProvider.now(),
                                                file: file,
                                                function: function,
                                                line: line)
        
        writer.write(message: formattedMessage)
    }
}

private class MultipleLogWriter: LogWriter {

    private var writers: [LogWriter]
    
    init(writers: [LogWriter] ) {
        self.writers = writers
    }
    
    func add(_ writer: LogWriter) {
        writers.append(writer)
    }
    
    func write(message: String) {
        
        for writer in writers {
            writer.write(message: message)
        }
    }
    
    func flush() {
        for writer in writers {
            writer.flush()
        }
    }
}

extension Logger.Level: Comparable {
    public static func <(lhs: Logger.Level, rhs: Logger.Level) -> Bool {
        return lhs.rawValue < rhs.rawValue
    }
}
