// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

public enum PathUtils {
    
    public static func name(path: String) -> String {
        
        let trimmedPath = path.trimmingCharacters(in: CharacterSet(charactersIn: .pathSeparator))
        
        guard let lastIndexOfSeparator = trimmedPath.lastIndex(of: .pathSeparator) else { return trimmedPath }
        
        return String(trimmedPath.suffix(from: trimmedPath.index(after: lastIndexOfSeparator)))
    }
}

extension Character {
    
    public static let pathSeparator: Character = "/"
}

extension String {
    
    public static let pathSeparator: String = "/"
}
