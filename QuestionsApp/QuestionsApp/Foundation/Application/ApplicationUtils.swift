// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class ApplicationUtils {

    static func openUrl(_ urlString: String) {

        let application = UIApplication.shared
        if let url = URL(string: urlString), application.canOpenURL(url) {
            application.open(url, options: [:], completionHandler: nil)
        }
    }
}
