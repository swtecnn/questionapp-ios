// Copyright © 2018 SWTecNN. All rights reserved.

import UIKit
import MBProgressHUD

class BaseViewController: UIViewController, IModuleTransitionHandler, IModuleInputReceiver {
    
    var moduleInput: Any?
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let transitionTarget = segue.destination as? IModuleInputReceiver,
            let moduleInput = transitionTarget.moduleInput,
            let transitionBlock = sender as? TransitionBlockType {
            
            transitionBlock(moduleInput)
        }
    }

    //MARK: IModuleTransitionHandler
    func performTransition(_ identifier: String) {
        
        self.performSegue(withIdentifier: identifier, sender: self)
    }
    
    func openModule(id: String, with transition: @escaping (Any) -> Void) {
        
        self.performSegue(withIdentifier: id, sender: transition)
    }
    
    func close() {
        
        if let navigation = navigationController {
            navigation.popViewController(animated: true)
        } else {
            self.dismiss(animated: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        hideProgress()
    }
    
    func handleError(_ error: Error) {
        showErrorMessage(error.localizedDescription)
    }
}

extension BaseViewController: IProgressableView {
    
    func showProgress() {
        MBProgressHUD.showAdded(to: view, animated: true)
    }
    
    func hideProgress() {
        MBProgressHUD.hide(for: view, animated: true)
    }
}

extension BaseViewController: IErrorView {
    
}
