// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol IProgressableView {

    func showProgress()
    func hideProgress()
}
