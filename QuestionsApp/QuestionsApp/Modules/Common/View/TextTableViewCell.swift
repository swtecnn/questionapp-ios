// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class TextTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!

    func setTitle(_ title: String?) {

        titleLabel.text = title
    }
}
