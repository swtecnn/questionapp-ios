// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

extension UIAlertController {
    
    func setTitleFont(_ font: UIFont, color: UIColor) {
        guard let title = self.title else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        
        let attributes = [
            NSAttributedString.Key.font : font,
            NSAttributedString.Key.foregroundColor : color,
            NSAttributedString.Key.paragraphStyle : paragraphStyle
        ];
        
        let attributeString = NSAttributedString(string: title, attributes: attributes)
        
        setValue(attributeString, forKey: "attributedTitle")
    }
    
    func setMessageFont(_ font: UIFont, color: UIColor) {
        guard let message = self.message else { return }
        
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = .left
        
        let attributes = [
            NSAttributedString.Key.font : font,
            NSAttributedString.Key.foregroundColor : color,
            NSAttributedString.Key.paragraphStyle : paragraphStyle
        ];
        
        let attributeString = NSAttributedString(string: message, attributes: attributes)
        
        setValue(attributeString, forKey: "attributedMessage")
    }
}
