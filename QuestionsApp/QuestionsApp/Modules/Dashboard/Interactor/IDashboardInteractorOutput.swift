//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol IDashboardInteractorOutput: class {

    func nameObtained(_ name: String)
    func nameObtainError(_ error: Error)
    func nameSaved()
    func nameSaveError(_ error: Error)
    func socialEventsObtained(_ socialEvents: [SocialEventModel])
    func socialEventsObtainError(_ error: Error)
    func socialEventObtained(_ socialEvent: SocialEvent)
    func socialEventObtainError(_ error: Error)
}
