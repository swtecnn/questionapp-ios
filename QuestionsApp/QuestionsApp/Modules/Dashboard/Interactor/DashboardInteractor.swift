//  Copyright © 2019 SWTecNN. All rights reserved.

class DashboardInteractor: IDashboardInteractor {

    weak var output: IDashboardInteractorOutput!

    let clientSessionRepository: ClientSessionRepository
    let clientSocialEventRepository: ClientSocialEventRepository
    let socialEventRepository: SocialEventRepository

    init(clientSessionRepository: ClientSessionRepository,
         clientSocialEventRepository: ClientSocialEventRepository,
         socialEventRepository: SocialEventRepository) {

        self.clientSessionRepository = clientSessionRepository
        self.clientSocialEventRepository = clientSocialEventRepository
        self.socialEventRepository = socialEventRepository
    }

    func obtainName() {

        async {
            do {
                let name = try self.clientSessionRepository.readName()
                asyncMain {
                    self.output.nameObtained(name)
                }
            } catch {
                asyncMain {
                    self.output.nameObtainError(error)
                }
            }
        }
    }

    func saveName(_ name: String) {

        async {
            do {
                try self.clientSessionRepository.saveName(name)
                asyncMain {
                    self.output.nameSaved()
                }
            } catch {
                asyncMain {
                    self.output.nameSaveError(error)
                }
            }
        }
    }

    func obtainSocialEvents() {

        async {
            do {
                let clientSocialEvents = try self.clientSocialEventRepository.getArchivedSocialEvents()
                var socialEvents = [SocialEventModel]()
                for event in clientSocialEvents {
                    let socialEvent = try self.socialEventRepository.getSocialEvent(with: event.id)
                    socialEvents.append(SocialEventModel(socialEvent: socialEvent, role: event.role))
                }
                asyncMain {
                    self.output.socialEventsObtained(socialEvents)
                }
            } catch {
                asyncMain {
                    self.output.socialEventsObtainError(error)
                }
            }
        }
    }
    
    func obtainSocialEvent(with socialEventId: String) {
        
        async {
            do {
                let socialEvent = try self.socialEventRepository.getSocialEvent(with: socialEventId)
                asyncMain {
                    self.output.socialEventObtained(socialEvent)
                }
            } catch {
                asyncMain {
                    self.output.socialEventObtainError(error)
                }
            }
        }
    }

}
