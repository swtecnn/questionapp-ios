// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class SocialEventTableViewController: UITableViewController {

    private let socialEventCellId = "SocialEventCell"

    var socialEvents: [SocialEventModel]!
    var onSocialEventSelected: ((SocialEvent) -> Void)?
}

extension SocialEventTableViewController {

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return socialEvents.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: socialEventCellId, for: indexPath) as? SocialEventTableViewCell else {
            return UITableViewCell()
        }

        cell.setSocialEvent(socialEvents[indexPath.row])

        return cell
    }
}

extension SocialEventTableViewController {

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        onSocialEventSelected?(socialEvents[indexPath.row].socialEvent)
    }
}
