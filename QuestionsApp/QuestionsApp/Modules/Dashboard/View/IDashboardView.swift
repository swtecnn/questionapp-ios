//  Copyright © 2019 SWTecNN. All rights reserved.

protocol IDashboardView: class, IProgressableView, IErrorView {

    func showName(_ name: String)
    func setSocialEvents(_ socialEvents: [SocialEventModel])
    func showSocialEvents(_ socialEvents: [SocialEventModel])
}
