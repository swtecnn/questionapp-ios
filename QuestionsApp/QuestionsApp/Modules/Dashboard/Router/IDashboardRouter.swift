//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol IDashboardRouter {
    
    func openSocialEventInfo(with id: String, role: Role)
    func openSocialEvent(with id: String, role: Role)
    func openAuthentication()
}
