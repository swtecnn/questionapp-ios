//  Copyright © 2019 SWTecNN. All rights reserved.

class SocialEventQuestionsPresenter: ISocialEventQuestionsModule {

    weak var view: ISocialEventQuestionsView!
    var interactor: ISocialEventQuestionsInteractor!
    var router: ISocialEventQuestionsRouter!

    var socialEventId: String!
    var role: Role!
    
    private var questions = [SocialEventQuestion]()
    
    private func sortQuestions() {
        questions = sortQuestions(questions)
    }
    
    private func sortQuestions(_ questions: [SocialEventQuestion]) -> [SocialEventQuestion] {
        return questions.sorted {
            if $0.archived == $1.archived {
                return $0.voteCount > $1.voteCount
            }
            return !$0.archived && $1.archived
        }
    }
    
    private func deleteQuestion(with questionId: String) {
        
        interactor.deleteQuestion(with: questionId)
        
        guard let index = questions.firstIndex(where: {$0.id == questionId}) else {
            return
        }
        
        questions.remove(at: index)
        
        view.updateQuestions(questions, andRemoveAt: index)
    }
    
    private func updateQuestion(for questionId: String, action: (SocialEventQuestion) -> Void) {
        
        guard let index = questions.firstIndex(where: {$0.id == questionId}) else {
            return
        }
        
        let question = questions[index]
        action(question)
        sortQuestions()
        
        guard let newIndex = questions.firstIndex(where: {$0.id == questionId}) else {
            return
        }
        
        if index == newIndex {
            view.reloadQuestion(at: index)
        } else {
            view.updateQuestions(questions, andMoveAt: index, to: newIndex)
        }
    }
}

extension SocialEventQuestionsPresenter: ISocialEventQuestionsInteractorOutput {
    
    func questionsObtained(_ questions: [SocialEventQuestion]) {
        self.questions = sortQuestions(questions)
        view.showQuestions(self.questions, for: role)
        view.hideProgress()
    }
    
    func questionsObtainError(_ error: Error) {
        view.hideProgress()
        view.handleError(error)
    }
    
    func updateVoteError(_ error: Error, for questionId: String) {
        view.handleError(error)
    }
    
    func deleteQuestionError(_ error: Error, for questionId: String) {
        view.handleError(error)
    }
    
    func archiveQuestionError(_ error: Error, for questionId: String) {
        view.handleError(error)
    }
}

extension SocialEventQuestionsPresenter: ISocialEventQuestionsViewOutput {

    func viewIsReady() {
        view.showProgress()
        interactor.obtainQuestions(for: socialEventId)
    }
    
    func updateVotePressed(for question: SocialEventQuestion) {

        let vote = question.voted ? -1 : 1
        interactor.updateVoteForQuestion(with: question.id, vote: vote)
        
        updateQuestion(for: question.id) { question in
            question.voted = !question.voted
            question.voteCount += vote
        }
    }
    
    func revokePressed(for questionId: String) {
        deleteQuestion(with: questionId)
    }
    
    func deletePressed(for questionId: String) {
        deleteQuestion(with: questionId)
    }
    
    func archivePressed(for questionId: String) {
        
        interactor.archiveQuestion(with: questionId)
        
        updateQuestion(for: questionId) { question in
            question.archived = !question.archived
        }
    }
    
    func questionSelected(at index: Int) {
        view.showQuestion(at: index)
    }
}
