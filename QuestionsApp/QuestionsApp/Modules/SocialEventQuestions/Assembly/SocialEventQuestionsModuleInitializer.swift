//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class SocialEventQuestionsModuleInitializer: NSObject {

    @IBOutlet weak var socialEventQuestionsViewController: SocialEventQuestionsViewController!

    override func awakeFromNib() {

        let assembly = SocialEventQuestionsAssembly()
        assembly.assemblyModuleForView(view: socialEventQuestionsViewController)
    }

}
