//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol ISocialEventQuestionsInteractor {
    func obtainQuestions(for eventId: String)
    func updateVoteForQuestion(with questionId: String, vote: Int)
    func deleteQuestion(with questionId: String)
    func archiveQuestion(with questionId: String)
}
