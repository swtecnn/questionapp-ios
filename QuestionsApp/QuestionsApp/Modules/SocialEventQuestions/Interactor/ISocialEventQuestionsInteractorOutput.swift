//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol ISocialEventQuestionsInteractorOutput: class {
    func questionsObtained(_ questions: [SocialEventQuestion])
    func questionsObtainError(_ error: Error)
    func updateVoteError(_ error: Error, for questionId: String)
    func deleteQuestionError(_ error: Error, for questionId: String)
    func archiveQuestionError(_ error: Error, for questionId: String)
}
