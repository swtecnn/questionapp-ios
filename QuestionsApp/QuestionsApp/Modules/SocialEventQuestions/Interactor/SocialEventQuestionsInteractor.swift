//  Copyright © 2019 SWTecNN. All rights reserved.

class SocialEventQuestionsInteractor: ISocialEventQuestionsInteractor {

    weak var output: ISocialEventQuestionsInteractorOutput!
    
    private let questionRepository: QuestionRepository
    private let clientQuestionRepository: ClientQuestionRepository
    
    private let logger = Logger.factory.getLogger(for: .default)
    
    init(questionRepository: QuestionRepository,
         clientQuestionRepository: ClientQuestionRepository) {
        self.questionRepository = questionRepository
        self.clientQuestionRepository = clientQuestionRepository
    }
    
    func obtainQuestions(for eventId: String) {
        async {
            do {
                let questions = try self.questionRepository.getQuestions(for: eventId)
                let clientQuestions = try self.clientQuestionRepository.getQuestions()
                let votedQuestions = try self.clientQuestionRepository.getVotedQuestions()
                
                let socialEventQuestions = questions.compactMap { question -> SocialEventQuestion? in
                    guard let questionId = question.id else { return nil }
                    return SocialEventQuestion(id: questionId,
                                               socialEventId: question.socialEventId,
                                               author: question.name,
                                               description: question.description,
                                               voteCount: question.votes,
                                               voted: votedQuestions.contains(questionId),
                                               own: clientQuestions.contains(questionId),
                                               archived: question.archived)
                }
                asyncMain {
                    self.output.questionsObtained(socialEventQuestions)
                }
            } catch {
                asyncMain {
                    self.output.questionsObtainError(error)
                }
            }
        }
    }
    
    func updateVoteForQuestion(with questionId: String, vote: Int) {
        async {
            do {
                try self.questionRepository.updateVoteForQuestion(with: questionId, vote: vote)
                try self.clientQuestionRepository.updateVote(for: questionId)
            } catch {
                asyncMain {
                    self.output.updateVoteError(error, for: questionId)
                }
                self.logger.error("Unable to update vote for question", error: error)
            }
        }
    }
    
    func deleteQuestion(with questionId: String) {
        async {
            do {
                try self.questionRepository.deleteQuestion(with: questionId)
                try self.clientQuestionRepository.deleteQuestion(with: questionId)
            } catch {
                asyncMain {
                    self.output.deleteQuestionError(error, for: questionId)
                }
                self.logger.error("Unable to delete question", error: error)
            }
        }
    }
    
    func archiveQuestion(with questionId: String) {
        async {
            do {
                try self.questionRepository.archiveQuestion(with: questionId)
                try self.clientQuestionRepository.deleteQuestion(with: questionId)
            } catch {
                asyncMain {
                    self.output.archiveQuestionError(error, for: questionId)
                }
                self.logger.error("Unable to archive question", error: error)
            }
        }
    }
}
