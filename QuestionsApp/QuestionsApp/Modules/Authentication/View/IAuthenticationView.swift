//  Copyright © 2019 SWTecNN. All rights reserved.

protocol IAuthenticationView: class, IProgressableView, IErrorView {

    func startScanning()
    func stopScanning()
    func showPassword()
    func dismissAuthentication()
    func showIncorrectPasswordError()
}
