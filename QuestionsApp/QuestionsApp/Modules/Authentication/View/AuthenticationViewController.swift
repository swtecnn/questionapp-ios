//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit
import AVFoundation

class AuthenticationViewController: BaseViewController {

    @IBOutlet weak var qrCodeView: UIImageView!

    var output: IAuthenticationViewOutput!

    let captureSession = AVCaptureSession()
    var previewLayer: AVCaptureVideoPreviewLayer?

    override func viewDidLoad() {
        super.viewDidLoad()

        setupCaptureSession()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        previewLayer?.removeFromSuperlayer()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        output.viewIsShown()
    }

    override func viewWillDisappear(_ animated: Bool) {

        output.viewIsHidden()

        super.viewWillDisappear(animated)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        setupQRCodeView()
    }

    private func setupCaptureSession() {

        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video),
            let videoInput = try? AVCaptureDeviceInput(device: videoCaptureDevice),
            captureSession.canAddInput(videoInput) else {
                return
        }
        captureSession.addInput(videoInput)

        let metadataOutput = AVCaptureMetadataOutput()
        guard captureSession.canAddOutput(metadataOutput) else {
            return
        }
        captureSession.addOutput(metadataOutput)
        metadataOutput.setMetadataObjectsDelegate(self, queue: .main)
        metadataOutput.metadataObjectTypes = [.qr]

        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer?.videoGravity = .resizeAspectFill
    }

    private func setupQRCodeView() {

        previewLayer?.frame = qrCodeView.layer.bounds
    }
}

extension AuthenticationViewController: IAuthenticationView {

    func startScanning() {

        if let preview = previewLayer, !captureSession.isRunning {
            captureSession.startRunning()
            qrCodeView.layer.addSublayer(preview)
        }
    }

    func stopScanning() {

        if captureSession.isRunning {
            captureSession.stopRunning()
        }
    }

    func showPassword() {

        performSegue(withIdentifier: SegueIdentifier.showPassword, sender: nil)
    }

    func dismissAuthentication() {

        navigationController?.viewControllers.removeAll(where: { $0 is AuthenticationViewController || $0 is PasswordViewController })
    }

    func showIncorrectPasswordError() {

        showErrorMessage(lcMsg("incorrect_password"))
    }
}

extension AuthenticationViewController: IAuthenticationRouter {

    struct SegueIdentifier {
        static let showPassword = "showPassword"
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        switch segue.identifier {
        case SegueIdentifier.showPassword:

            guard let passwordView = segue.destination as? PasswordViewController else {
                break
            }

            passwordView.onResult = { [unowned self] password in
                self.output.submitPasswordPressed(password)
            }
            
        default:
            break
        }
    }
    
    func openSocialEventAdmin(id: String) {
        if let event = SocialViewFactory.getSocialEventView(with: id, role: .admin) {
            
            navigationController?.pushViewController(event, animated: true)
        }
    }
    
    func openSocialEventClient(id: String) {
        
        if let event = SocialViewFactory.getSocialEventView(with: id, role: .client) {
            
            navigationController?.pushViewController(event, animated: true)
        }
    }
    
    func openSocialEventViewer(id: String) {
        
        if let info = SocialViewFactory.getSocialEventView(with: id, role: .viewer) {

            navigationController?.pushViewController(info, animated: true)
        }
    }
}

extension AuthenticationViewController: AVCaptureMetadataOutputObjectsDelegate {

    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {

        if let code = (metadataObjects.first as? AVMetadataMachineReadableCodeObject)?.stringValue {
            self.output.qrCodeScanned(code)
        }
    }
}
