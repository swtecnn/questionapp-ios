//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol IAuthenticationRouter {

    func openSocialEventClient(id: String)
    func openSocialEventAdmin(id: String)
    func openSocialEventViewer(id: String)
}
