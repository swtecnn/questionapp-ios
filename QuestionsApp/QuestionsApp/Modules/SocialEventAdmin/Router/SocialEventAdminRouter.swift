//  Copyright © 2019 SWTecNN. All rights reserved.

class SocialEventAdminRouter: ISocialEventAdminRouter {
    
    var transition: IModuleTransitionHandler!
    
    func close() {
        transition.close()
    }
    
    func showSocialEventInfo(with id: String) {

        transition.openModule(id: "showSocialEventInfo") { (module) in
            guard let input = module as? ISocialEventInfoModule else { return }
            
            input.socialEventId = id
            input.role = .admin
        }
    }
    
    func showQuestionsFor(socialId: String) {
        
        transition.openModule(id: "showSocialEventQuestions") { (module) in
            guard let input = module as? ISocialEventQuestionsModule else { return }
            
            input.socialEventId = socialId
            input.role = .admin
        }
    }

}
