//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class SocialEventAdminAssembly {

    func assemblyModuleForView<UIViewController>(view: UIViewController) {

        if let viewController = view as? SocialEventViewController {
            assembly(viewController: viewController)
        }
    }

    private func assembly(viewController: SocialEventViewController) {

        let router = SocialEventAdminRouter()
        router.transition = viewController

        let presenter = SocialEventAdminPresenter()
        presenter.view = viewController
        presenter.router = router
        
        let eventRepository = Service.factory.getSocialEventRepository()
        let questionRepository = Service.factory.getQuestionRepository()
        let raiseHandRepository = Service.factory.getRaiseHandRepository()
        let clientEventRepository = Service.factory.getClientSocialEventRepository()
        let messagingService = Service.factory.getMessagingService()

        let interactor = SocialEventAdminInteractor(socialEventRepository: eventRepository,
                                                    raiseHandRepository: raiseHandRepository,
                                                    questionRepository: questionRepository,
                                                    clientSocialEventRepository: clientEventRepository,
                                                    messagingService: messagingService)
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.adminOutput = presenter
        viewController.moduleInput = presenter
    }

}
