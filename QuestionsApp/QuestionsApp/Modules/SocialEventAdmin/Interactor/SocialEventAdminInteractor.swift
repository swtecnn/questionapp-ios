//  Copyright © 2019 SWTecNN. All rights reserved.

class SocialEventAdminInteractor: ISocialEventAdminInteractor {

    weak var output: ISocialEventAdminInteractorOutput!
    
    private let socialEventRepository: SocialEventRepository
    private let raiseHandRepository: RaiseHandRepository
    private let questionRepository: QuestionRepository
    private let clientSocialEventRepository: ClientSocialEventRepository
    private let messagingService: IMessagingService
    
    private var socialEventId: String?
    
    private let logger = Logger.factory.getLogger(for: .default)
    
    init(socialEventRepository: SocialEventRepository,
         raiseHandRepository: RaiseHandRepository,
         questionRepository: QuestionRepository,
         clientSocialEventRepository: ClientSocialEventRepository,
         messagingService: IMessagingService) {
        self.socialEventRepository = socialEventRepository
        self.raiseHandRepository = raiseHandRepository
        self.questionRepository = questionRepository
        self.clientSocialEventRepository = clientSocialEventRepository
        self.messagingService = messagingService
    }

    func obtainSocialEvent(with id: String) {
        async {
            do {
                let socialEvent = try self.socialEventRepository.getSocialEvent(with: id)
                asyncMain {
                    self.output.socialEventObtained(socialEvent)
                }
            } catch {
                asyncMain {
                    self.output.socialEventObtainError(error)
                }
            }
        }
    }
    
    func finishSocialEvent(with id: String) {
        
        async {
            do {
                try self.socialEventRepository.archiveSocialEvent(with: id)
                try self.clientSocialEventRepository.archive(socialEvent: ClientSocialEvent(id: id, role: .lector))
                asyncMain {
                    self.output.socialEventFinished()
                }
            } catch {
                self.logger.error("Unable to archive social event: \(id)", error: error)
            }
        }
    }
    
    func obtainNextHandRequestForSocialEvent(id: String) {
        
        async {
            do {
                var requests = try self.raiseHandRepository.getRaiseHandRequests(for: id)
                requests.sort(by: { $0.timestamp > $1.timestamp })
                if let next = requests.last {
                    asyncMain {
                        self.output.handRequestObtained(next)
                    }
                } else {
                    throw RaiseHandRepositoryError.empty
                }
            } catch {
                asyncMain {
                    self.output.handRequestError(error)
                }
            }
        }
    }
    
    func notifyHandRequest(_ handRequest: RaiseHandRequest) {
        async {
            do {
                let message = Message(message: String(format: lcMsg("raise_hand_request_notification_message"), handRequest.name),
                                      title: lcMsg("raise_hand_request_notification_title"),
                                      token: handRequest.pushNotificationToken)
                try self.messagingService.send(message: message)
            } catch {
                self.logger.error("Unable send ready for question request", error: error)
            }
        }
    }
    
    func finishHandRequest(id: String) {
        async {
            do {
                try self.raiseHandRepository.deleteRaiseHandRequest(id: id)
            } catch {
                self.logger.error("Unable to delete request: \(id)", error: error)
                asyncMain {
                    self.output.handRequestError(error)
                }
            }
        }
    }
    
    func obtainHandRequestsCountForSocialEvent(id: String) {
        socialEventId = id
        async {
            do {
                self.raiseHandRepository.addListener(self)
                let count = try self.raiseHandRepository.getRaiseHandRequests(for: id).count
                asyncMain {
                    self.output.handRequestCountObtained(count)
                }
            } catch {
                self.logger.error("Unable to get hand requests count", error: error)
            }
            
        }
    }
    
    func obtainQuestionsCountForSocialEvent(id: String) {
        socialEventId = id
        async {
            do {
                self.questionRepository.addListener(self)
                let questions = try self.questionRepository.getQuestions(for: id)
                let count = questions.filter({ !$0.archived }).count
                asyncMain {
                    self.output.questionCountObtained(count)
                }
            } catch {
                self.logger.error("Unable to get questions count", error: error)
            }
        }
    }
}

extension SocialEventAdminInteractor: RaiseHandRepositoryListener {
    
    func requestCountChanged(socialEventId: String, count: Int) {

        guard let subscribedSocialEvent = self.socialEventId,
            subscribedSocialEvent == socialEventId else { return }
        
        asyncMain {
            self.output.handRequestCountObtained(count)
        }
    }
}

extension SocialEventAdminInteractor: QuestionRepositoryListener {
    
    func questionsCountChanged(socialEventId: String, count: Int) {
        
        guard let subscribedSocialEvent = self.socialEventId,
            subscribedSocialEvent == socialEventId else { return }
        
        asyncMain {
            self.output.questionCountObtained(count)
        }
    }
}
