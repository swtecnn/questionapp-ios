//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol ISocialEventAdminInteractor {
    
    func obtainSocialEvent(with id: String)
    func finishSocialEvent(with id: String)
    
    func obtainNextHandRequestForSocialEvent(id: String)
    func notifyHandRequest(_ handRequest: RaiseHandRequest)
    func finishHandRequest(id: String)
    
    func obtainQuestionsCountForSocialEvent(id: String)
    func obtainHandRequestsCountForSocialEvent(id: String)
}
