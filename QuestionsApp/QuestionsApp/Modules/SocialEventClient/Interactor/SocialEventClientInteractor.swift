//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

class SocialEventClientInteractor: ISocialEventClientInteractor {

    weak var output: ISocialEventClientInteractorOutput!
    
    let socialEventRepository: SocialEventRepository
    let questionRepository: QuestionRepository
    let clientQuestionRepository: ClientQuestionRepository
    let raiseHandRepository: RaiseHandRepository
    let deviceInfo: DeviceInfoProvider
    let clientSessionRepository: ClientSessionRepository
    let clientSocialEventRepository: ClientSocialEventRepository
    
    let logger = Logger.factory.getLogger(for: .default)
    
    init(socialEventRepository: SocialEventRepository,
         questionRepository: QuestionRepository,
         clientQuestionRepository: ClientQuestionRepository,
         raiseHandRepository: RaiseHandRepository,
         deviceInfo: DeviceInfoProvider,
         clientSessionRepository: ClientSessionRepository,
         clientSocialEventRepository: ClientSocialEventRepository) {
        self.socialEventRepository = socialEventRepository
        self.questionRepository = questionRepository
        self.clientQuestionRepository = clientQuestionRepository
        self.raiseHandRepository = raiseHandRepository
        self.deviceInfo = deviceInfo
        self.clientSessionRepository = clientSessionRepository
        self.clientSocialEventRepository = clientSocialEventRepository
    }
    
    func obtainSocialEvent(with id: String) {
        
        async {
            do {
                let socialEvent = try self.socialEventRepository.getSocialEvent(with: id)
                asyncMain {
                    self.output.socialEventObtained(socialEvent)
                }
            } catch {
                asyncMain {
                    self.output.socialEventObtainError(error)
                }
            }
        }
    }
    
    func archiveSocialEvent(with id: String) {
        
        async {
            do {
                try self.clientSocialEventRepository.archive(socialEvent: ClientSocialEvent(id: id, role: .listener))
            } catch {
                self.logger.error("Unable to archive socialEvent", error: error)
            }
        }
    }
    
    func createQuestion(_ question: Question) {
        
        async {
            do {
                let questionId = try self.questionRepository.addQuestion(question)
                try self.clientQuestionRepository.addQuestion(id: questionId)
                asyncMain {
                    self.output.questionCreated()
                }
            } catch {
                self.logger.error("unable to create question", error: error)
                asyncMain {
                    self.output.questionCreationError(error)
                }
            }
        }
    }
    
    func createRaiseHandRequest(for socialEventId: String) {
        
        async {
            do {
                if let requestId = try self.clientSocialEventRepository.getRaiseHandRequestId(for: socialEventId) {
                    do {
                        let requests = try self.raiseHandRepository.getRaiseHandRequests(for: socialEventId)
                        if requests.firstIndex(where: { $0.id == requestId }) != nil  {
                            self.obtainRaiseHandRequestStatus(id: requestId, socialEventId: socialEventId)
                            return
                        } else {
                            try self.clientSocialEventRepository.deleteRaiseHandRequest(for: socialEventId)
                        }
                    } catch {
                        self.logger.error("unable to get raise hand request", error: error)
                    }
                }
                
                let token = try self.deviceInfo.getPushToken()
                let name = try self.clientSessionRepository.readName()
                let request = RaiseHandRequest(id: nil,
                                               socialEventId: socialEventId,
                                               pushNotificationToken: token,
                                               name: name,
                                               timestamp: Date())
                let requestId = try self.raiseHandRepository.addRaiseHandRequest(request)
                try self.clientSocialEventRepository.addRaiseHandRequest(with: requestId, for: socialEventId)
                self.obtainRaiseHandRequestStatus(id: requestId, socialEventId: socialEventId)
                
            } catch {
                self.logger.error("unable to create raise hand request", error: error)
                asyncMain {
                    self.output.raiseHandRequestError(error)
                }
            }
        }
    }
    
    func obtainRaiseHandRequestStatus(for socialEventId: String) {
        
        async {
            do {
                if let requestId = try self.clientSocialEventRepository.getRaiseHandRequestId(for: socialEventId) {
                    self.obtainRaiseHandRequestStatus(id: requestId, socialEventId: socialEventId)
                } else {
                    throw RaiseHandRepositoryError.unableToGet
                }
            } catch {
                self.logger.error("unable to get raise hand request status", error: error)
                asyncMain {
                    self.output.raiseHandRequestError(error)
                }
            }
        }
    }
    
    private func obtainRaiseHandRequestStatus(id: String, socialEventId: String) {
        async {
            do {
                let requests = try self.raiseHandRepository.getRaiseHandRequests(for: socialEventId).sorted(by: { $0.timestamp < $1.timestamp })
                if let index = requests.firstIndex(where: { $0.id == id }) {
                    let status = RaiseHandRequestStatus(order: index, state: index > 0 ? .inQueue : .active)
                    asyncMain {
                        self.output.raiseHandRequestStatusObtained(status)
                    }
                } else {
                    try self.clientSocialEventRepository.deleteRaiseHandRequest(for: socialEventId)
                    asyncMain {
                        self.output.raiseHandRequestError(RaiseHandRepositoryError.unableToGet)
                    }
                }
            } catch {
                self.logger.error("unable to get raise hand request status", error: error)
                asyncMain {
                    self.output.raiseHandRequestError(error)
                }
            }
        }
    }
    
    func cancelRaiseHandRequest(for socialEventId: String) {
        async {
            do {
                if let id = try self.clientSocialEventRepository.getRaiseHandRequestId(for: socialEventId) {
                    try self.raiseHandRepository.deleteRaiseHandRequest(id: id)
                    try self.clientSocialEventRepository.deleteRaiseHandRequest(for: socialEventId)
                }
                asyncMain {
                    self.output.raiseHandRequestCancelled()
                }
            } catch {
                self.logger.error("unable to cancel raise hand request", error: error)
                asyncMain {
                    self.output.raiseHandRequestError(error)
                }
            }
        }
    }
    
    func obtainName() {
        async {
            do {
                let name = try self.clientSessionRepository.readName()
                asyncMain {
                    self.output.nameObtained(name)
                }
            } catch {
                self.logger.error("Unable to get name", error: error)
            }
        }
    }

}
