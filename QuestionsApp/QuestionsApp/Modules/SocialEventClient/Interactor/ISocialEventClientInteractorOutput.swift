//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol ISocialEventClientInteractorOutput: class {

    func socialEventObtained(_ socialEvent: SocialEvent)
    func socialEventObtainError(_ error: Error)
    
    func nameObtained(_ name: String)
    
    func questionCreated()
    func questionCreationError(_ error: Error)
    
    func raiseHandRequestError(_ error: Error)
    func raiseHandRequestStatusObtained(_ status: RaiseHandRequestStatus)
    func raiseHandRequestCancelled()
}
