//  Copyright © 2019 SWTecNN. All rights reserved.

protocol ISocialEventClientView: class, IProgressableView, IErrorView {

    func showSocialEventInfo(_ socialEvent: SocialEvent)
    func showRaiseHandRequest(status: RaiseHandRequestStatus)
    func hideRaiseHandRequestStatus()
    func setDefaultName(_ name: String)
}
