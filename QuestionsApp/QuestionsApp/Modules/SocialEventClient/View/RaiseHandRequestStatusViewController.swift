// Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class RaiseHandRequestStatusViewController: UIViewController {
    
    @IBOutlet var statusLabel: UILabel?
    @IBOutlet var queueLabel: UILabel?
    
    public var status: RaiseHandRequestStatus? {
        didSet {
            updateStatus()
        }
    }
    
    public var onCancel: (()-> Void)?
    public var onRefresh: (()-> Void)?
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateStatus()
    }
    
    func updateStatus() {
        guard let status = status else { return }
        
        statusLabel?.text = getStatusText(for: status.state)
        queueLabel?.text = "\(status.order)"
    }
    
    private func getStatusText(for state: RaiseHandRequestStatus.State) -> String {

        switch state {
        case .active:
            return "raise_hand_request_status_active".lcMsg()
        default:
            return "raise_hand_request_status_inQueue".lcMsg()
        }
    }

    @IBAction func cancelPressed() {
        onCancel?()
    }
    @IBAction func refreshPressed() {
        onRefresh?()
    }
}
