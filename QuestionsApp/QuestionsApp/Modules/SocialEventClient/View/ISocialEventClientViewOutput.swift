//  Copyright © 2019 SWTecNN. All rights reserved.

protocol ISocialEventClientViewOutput {

    func viewIsShown()
    func createQuestionPressed(name: String?, slide: Int?, question: String)
    func socialEventInfoPressed()
    func openQuestionsPressed()
    func raiseHandRequestPressed()
    func cancelHandRequestPressed()
    func updateHandRequestPressed()
}
