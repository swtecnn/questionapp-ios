//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class SocialEventClientAssembly {

    func assemblyModuleForView<UIViewController>(view: UIViewController) {

        if let viewController = view as? SocialEventViewController {
            assembly(viewController: viewController)
        }
    }

    private func assembly(viewController: SocialEventViewController) {

        let router = SocialEventClientRouter()
        router.transition = viewController

        let presenter = SocialEventClientPresenter()
        presenter.view = viewController
        presenter.router = router

        let socialEventRepository = Service.factory.getSocialEventRepository()
        let questionRepository = Service.factory.getQuestionRepository()
        let clientStorage = Service.factory.getClientQuestionRepository()
        let raiseHandRepository = Service.factory.getRaiseHandRepository()
        let deviceInfo = Service.factory.getDeviceInfoProvider()
        let clientSessionRepository = Service.factory.getClientSessionRepository()
        let clientSocialEventRepository = Service.factory.getClientSocialEventRepository()
        
        let interactor = SocialEventClientInteractor(socialEventRepository: socialEventRepository,
                                                     questionRepository: questionRepository,
                                                     clientQuestionRepository: clientStorage,
                                                     raiseHandRepository: raiseHandRepository,
                                                     deviceInfo: deviceInfo,
                                                     clientSessionRepository: clientSessionRepository,
                                                     clientSocialEventRepository: clientSocialEventRepository)
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.clientOutput = presenter
        viewController.moduleInput = presenter
    }

}
