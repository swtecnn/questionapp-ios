//  Copyright © 2019 SWTecNN. All rights reserved.

class SocialEventClientPresenter: ISocialEventClientModule {

    weak var view: ISocialEventClientView!
    var interactor: ISocialEventClientInteractor!
    var router: ISocialEventClientRouter!
    
    var socialEventId: String!
}

extension SocialEventClientPresenter: ISocialEventClientInteractorOutput {
    
    func raiseHandRequestError(_ error: Error) {
        
        defer {
            view.hideProgress()
        }
        
        if case RaiseHandRepositoryError.unableToGet = error {
            view.hideRaiseHandRequestStatus()
        } else {
            view.handleError(error)
        }
    }
    
    func raiseHandRequestStatusObtained(_ status: RaiseHandRequestStatus) {
        view.hideProgress()
        view.showRaiseHandRequest(status: RaiseHandRequestStatus(order: status.order + 1,
                                                                 state: status.state))
    }

    func raiseHandRequestCancelled() {
        view.hideRaiseHandRequestStatus()
        view.hideProgress()
    }
    
    func socialEventObtainError(_ error: Error) {
        view.hideProgress()
        view.handleError(error)
    }
    
    func socialEventObtained(_ socialEvent: SocialEvent) {
        interactor.archiveSocialEvent(with: socialEvent.id)
        view.showSocialEventInfo(socialEvent)
        view.hideProgress()
    }
    
    func questionCreated() {

        view.hideProgress()
    }
    
    func questionCreationError(_ error: Error) {
        view.handleError(error)
    }
    
    func nameObtained(_ name: String) {
        view.setDefaultName(name)
    }
}

extension SocialEventClientPresenter: ISocialEventClientViewOutput {
    
    func viewIsShown() {

        view.showProgress()
        interactor.obtainSocialEvent(with: socialEventId)
        interactor.obtainName()
    }
    
    func createQuestionPressed(name: String?, slide: Int?, question: String) {

        view.showProgress()
        interactor.createQuestion(Question(id: nil,
                                           name: name,
                                           slideNum: slide,
                                           socialEventId: socialEventId,
                                           description: question,
                                           votes: 0,
                                           archived: false))
    }
    
    func socialEventInfoPressed() {
        router.showSocialEventInfo(with: socialEventId)
    }
    
    func openQuestionsPressed() {
        router.showQuestionsFor(socialId: socialEventId)
    }
    
    func raiseHandRequestPressed() {
        
        view.showProgress()
        interactor.createRaiseHandRequest(for: socialEventId)
    }
    
    func cancelHandRequestPressed() {

        view.showProgress()
        interactor.cancelRaiseHandRequest(for: socialEventId)
    }
    
    func updateHandRequestPressed() {
        view.showProgress()
        interactor.obtainRaiseHandRequestStatus(for: socialEventId)
    }
    
}
