//  Copyright © 2019 SWTecNN. All rights reserved.

class SocialEventClientRouter: ISocialEventClientRouter {

    var transition: IModuleTransitionHandler!

    func showSocialEventInfo(with id: String) {
        
        transition.openModule(id: "showSocialEventInfo") { (module) in
            guard let input = module as? ISocialEventInfoModule else { return }
            
            input.socialEventId = id
            input.role = .client
        }
    }
    
    func showQuestionsFor(socialId: String) {
        
        transition.openModule(id: "showSocialEventQuestions") { (module) in
            guard let input = module as? ISocialEventQuestionsModule else { return }
            
            input.socialEventId = socialId
            input.role = .client
        }
    }
}
