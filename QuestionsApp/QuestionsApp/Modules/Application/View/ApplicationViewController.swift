//  Copyright © 2018 SWTecNN. All rights reserved.

import UIKit

class ApplicationViewController: BaseViewController, IApplicationView {

    var output: IApplicationViewOutput!

    // MARK: Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        asyncMain { [weak self] in
            self?.output.viewIsReady()
        }
    }


    // MARK: IApplicationView
    func setupInitialState() {
    }
    
    func showInitializationError() {
    }
}
