// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

class ApplicationStateMachine: IApplicationState {
    
    private var listeners = WeakArray<AnyObject>()
    
    var current: ApplicationState
    
    init() {
        current = .notStarted
    }
    
    func changeState(to newValue: ApplicationState) {
        
        guard current != newValue else { return }
        
        let previous = current
        current = newValue
        
        for listenerReference in listeners {
            
            if let listener = listenerReference as? IApplicationStateListener {
            
                listener.stateChanged(to: newValue, from: previous)
            }
        }
        
    }
    
    func addListener(_ listener: IApplicationStateListener) {
        
        listeners.append(listener)
    }
    
    func removeListener(_ listener: IApplicationStateListener) {
        
        listeners.remove(listener)
    }
}
