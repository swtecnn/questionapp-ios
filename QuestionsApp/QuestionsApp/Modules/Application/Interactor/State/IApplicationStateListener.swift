// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

protocol IApplicationStateListener: class {
    
    func stateChanged(to: ApplicationState, from: ApplicationState)
}
