//  Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

protocol IApplicationInteractorOutput: class {

    func applicationStateChanged(to: ApplicationState, from: ApplicationState)
}
