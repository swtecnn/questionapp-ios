//  Copyright © 2018 SWTecNN. All rights reserved.

class ApplicationPresenter: IApplicationModule {

    weak var view: IApplicationView!
    var interactor: IApplicationInteractor!
    var router: IApplicationRouter!

    func viewIsReady() {
        
        view.setupInitialState()
        
        do {
            try interactor.start()
        } catch {
            view.showInitializationError()
        }
    }
}

extension ApplicationPresenter: IApplicationInteractorOutput {
    
    func applicationStateChanged(to new: ApplicationState, from prev: ApplicationState) {
        
        switch (prev, new) {
        case (.starting, .running):
            view.hideProgress()
            router.showMain()
        case (.notStarted, .starting):
            view.showProgress()
        default:
            break
        }
    }
}

extension ApplicationPresenter: IApplicationViewOutput {
    
    func applicationDidEnterBackground() {
        interactor.enterBackground()
    }
    
    func applicationWillEnterForeground() {
        interactor.enterForeground()
    }
    
    func applicationDidBecomeActive() {
        interactor.becomeActive()
    }
    
    func applicationWillTerminate() {
        interactor.terminate()
    }
}
