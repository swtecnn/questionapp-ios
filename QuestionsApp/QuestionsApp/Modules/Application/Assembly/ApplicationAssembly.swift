//  Copyright © 2018 SWTecNN. All rights reserved.

import UIKit

class ApplicationAssembly {

    func assemblyModuleForView<UIViewController>(view: UIViewController) {

        if let viewController = view as? ApplicationViewController {
            assembly(viewController: viewController)
        }
    }

    private func assembly(viewController: ApplicationViewController) {

        var logger = Logger.factory.getLogger(for: .default)
        
        do {
            
            let logsDirectory = try FileManager.default
                .url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: true)
                .appendingPathComponent("logs", isDirectory: true)
            let logConfiguration = LogConfiguration(level: .info,
                                                    disabledDomains: [],
                                                    fileNamePrefix: "SWT",
                                                    logsDirectory: logsDirectory,
                                                    rollingFrequency: TimeInterval(minutes: 1),
                                                    maxFilesCount: 4)
            let logFactory = DefaultLoggerFactory(configuration: logConfiguration)
            Logger.factory = logFactory
            
            logger = Logger.factory.getLogger(for: .default)
            
            let state = ApplicationStateMachine()
            let serviceFactory = ServicesFactory()
            Service.factory = serviceFactory
            
            let analytics = Service.factory.getAnalyticsRegistry()
            
            let router = ApplicationRouter()
            router.transition = viewController
            
            let presenter = ApplicationPresenter()
            presenter.view = viewController
            presenter.router = router
            
            let interactor = ApplicationInteractor(state: state, analyticsRegistry: analytics)
            interactor.output = presenter
            
            presenter.interactor = interactor
            viewController.output = presenter
            if let appDelegate = UIApplication.shared.delegate as? AppDelegate {
                appDelegate.output = presenter
            }
            
            logger.info("App is started")
            
        } catch {
            logger.error("Unable to initalize application", error: error)
        }
    }

}
