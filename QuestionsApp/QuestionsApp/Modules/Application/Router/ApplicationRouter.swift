//  Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

class ApplicationRouter: IApplicationRouter {
    
    weak var transition: IModuleTransitionHandler?
    
    func showMain() {
        
        transition?.performTransition("showMain")
    }
}
