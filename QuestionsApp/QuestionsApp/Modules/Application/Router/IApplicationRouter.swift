//  Copyright © 2018 SWTecNN. All rights reserved.

import Foundation

protocol IApplicationRouter {

    func showMain()
}
