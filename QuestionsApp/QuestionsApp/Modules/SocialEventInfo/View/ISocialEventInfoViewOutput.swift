//  Copyright © 2019 SWTecNN. All rights reserved.

protocol ISocialEventInfoViewOutput {

    func viewIsReady()
    func socialEventMaterialPressed(_ material: String)
    func socialEventQuestionsPressed()
}
