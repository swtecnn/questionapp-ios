//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class SocialEventInfoModuleInitializer: NSObject {

    @IBOutlet weak var socialEventinfoViewController: SocialEventInfoViewController!

    override func awakeFromNib() {

        let assembly = SocialEventInfoAssembly()
        assembly.assemblyModuleForView(view: socialEventinfoViewController)
    }

}
