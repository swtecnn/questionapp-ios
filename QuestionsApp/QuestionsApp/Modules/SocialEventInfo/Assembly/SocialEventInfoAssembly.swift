//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class SocialEventInfoAssembly {

    func assemblyModuleForView<UIViewController>(view: UIViewController) {

        if let viewController = view as? SocialEventInfoViewController {
            assembly(viewController: viewController)
        }
    }

    private func assembly(viewController: SocialEventInfoViewController) {

        let router = SocialEventInfoRouter()
        router.transitionHandler = viewController

        let presenter = SocialEventInfoPresenter()
        presenter.view = viewController
        presenter.router = router

        let repository = Service.factory.getSocialEventRepository()

        let interactor = SocialEventInfoInteractor(socialEventRepository: repository)
        interactor.output = presenter

        presenter.interactor = interactor
        viewController.output = presenter
        viewController.moduleInput = presenter
    }

}
