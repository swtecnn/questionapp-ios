//  Copyright © 2019 SWTecNN. All rights reserved.

import UIKit

class SocialEventInfoPresenter: ISocialEventInfoModule {

    weak var view: ISocialEventInfoView!
    var interactor: ISocialEventInfoInteractor!
    var router: ISocialEventInfoRouter!

    var socialEventId: String!
    var role: Role!
}

extension SocialEventInfoPresenter: ISocialEventInfoInteractorOutput {

    func socialEventObtained(_ socialEvent: SocialEvent) {

        view.showSocialEventInfo(socialEvent)
        view.hideProgress()
    }

    func socialEventObtainError(_ error: Error) {
        
        view.hideProgress()
        view.handleError(error)
    }
}

extension SocialEventInfoPresenter: ISocialEventInfoViewOutput {

    func viewIsReady() {

        guard let socialEventId = socialEventId, let role = role else {
            return
        }
        
        if role == .viewer {
            view.enableQuestions()
        }

        view.showProgress()
        interactor.obtainSocialEvent(with: socialEventId)
    }

    func socialEventMaterialPressed(_ material: String) {

        router.openSocialEventMaterial(material)
    }

    func socialEventQuestionsPressed() {

        router.openSocialEventQuestions(with: socialEventId, role: role)
    }

}
