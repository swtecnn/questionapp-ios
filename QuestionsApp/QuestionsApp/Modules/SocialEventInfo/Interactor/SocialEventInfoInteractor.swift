//  Copyright © 2019 SWTecNN. All rights reserved.

class SocialEventInfoInteractor: ISocialEventInfoInteractor {

    weak var output: ISocialEventInfoInteractorOutput!

    let socialEventRepository: SocialEventRepository

    init(socialEventRepository: SocialEventRepository) {

        self.socialEventRepository = socialEventRepository
    }

    func obtainSocialEvent(with id: String) {

        async {
            do {
                let socialEvent = try self.socialEventRepository.getSocialEvent(with: id)
                asyncMain {
                    self.output.socialEventObtained(socialEvent)
                }
            } catch {
                asyncMain {
                    self.output.socialEventObtainError(error)
                }
            }
        }
    }
}
