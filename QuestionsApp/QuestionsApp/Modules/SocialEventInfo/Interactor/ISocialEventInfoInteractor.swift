//  Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

protocol ISocialEventInfoInteractor {

    func obtainSocialEvent(with id: String)
}
