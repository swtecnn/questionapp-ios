// Copyright © 2019 SWTecNN. All rights reserved.

import Foundation

extension String {
    
    func lcMsg() -> String {
        
        return NSLocalizedString(self, comment: "")
    }
}

func lcMsg(_ string: String) -> String {
    return string.lcMsg()
}
