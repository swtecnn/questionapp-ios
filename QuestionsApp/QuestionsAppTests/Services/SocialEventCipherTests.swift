// Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class SocialEventCipherTests: XCTestCase {
    
    var cipher: SocialEventCipher!

    override func setUp() {
        
        cipher = SocialEventCipher()
    }
    
    func test() {
        
        let code = cipher.encrypt(code: "2134")
        print(code)
        XCTAssert(cipher.decrypt(code: "2134", hash: code.hash) == code.code)
    }

}
