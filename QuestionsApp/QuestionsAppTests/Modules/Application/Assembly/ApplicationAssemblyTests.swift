//  Copyright © 2018 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class ApplicationModuleAssemblyTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAssemblyModuleForViewController() {

        //given
        let viewController = ApplicationViewControllerMock()
        let assembly = ApplicationAssembly()

        //when
        assembly.assemblyModuleForView(view: viewController)

        //then
        XCTAssertNotNil(viewController.output, "ApplicationViewController is nil after assembly")
        XCTAssertTrue(viewController.output is ApplicationPresenter, "output is not ApplicationPresenter")

        let presenter: ApplicationPresenter = viewController.output as! ApplicationPresenter
        XCTAssertNotNil(presenter.view, "view in ApplicationPresenter is nil after assembly")
        XCTAssertNotNil(presenter.router, "router in ApplicationPresenter is nil after assembly")
        XCTAssertTrue(presenter.router is ApplicationRouter, "router is not ApplicationRouter")

        let interactor: ApplicationInteractor = presenter.interactor as! ApplicationInteractor
        XCTAssertNotNil(interactor.output, "output in ApplicationInteractor is nil after assembly")
    }

    class ApplicationViewControllerMock: ApplicationViewController {

        var setupInitialStateDidCall = false

        override func setupInitialState() {
            setupInitialStateDidCall = true
        }
    }
}
