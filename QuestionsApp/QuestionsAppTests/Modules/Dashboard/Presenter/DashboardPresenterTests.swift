//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class DashboardPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: IDashboardInteractor {
        func obtainSocialEvent(with socialEventId: String) {
            
        }

        func obtainName() {
        }

        func saveName(_ name: String) {
        }

        func obtainSocialEvents() {
        }
    }

    class MockRouter: IDashboardRouter {
        func openSocialEvent(with id: String, role: Role) {
            
        }
        
        func openSocialEventInfo(with id: String, role: Role) {
        }

        func openAuthentication() {
        }
    }

    class MockViewController: IDashboardView {
        func showProgress() {
            
        }
        
        func hideProgress() {
            
        }

        func showName(_ name: String) {
        }

        func handleError(_ error: Error) {
        }

        func setSocialEvents(_ socialEvents: [SocialEventModel]) {
        }

        func showSocialEvents(_ socialEvents: [SocialEventModel]) {
        }
    }
}
