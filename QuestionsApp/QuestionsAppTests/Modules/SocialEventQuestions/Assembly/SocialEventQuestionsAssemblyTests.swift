//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class SocialEventQuestionsModuleAssemblyTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testAssemblyModuleForViewController() {

        //given
        let viewController = SocialEventQuestionsViewControllerMock()
        let assembly = SocialEventQuestionsAssembly()

        //when
        assembly.assemblyModuleForView(view: viewController)

        //then
        XCTAssertNotNil(viewController.output, "SocialEventQuestionsViewController is nil after assembly")
        XCTAssertTrue(viewController.output is SocialEventQuestionsPresenter, "output is not SocialEventQuestionsPresenter")

        let presenter: SocialEventQuestionsPresenter = viewController.output as! SocialEventQuestionsPresenter
        XCTAssertNotNil(presenter.view, "view in SocialEventQuestionsPresenter is nil after assembly")
        XCTAssertNotNil(presenter.router, "router in SocialEventQuestionsPresenter is nil after assembly")
        XCTAssertTrue(presenter.router is SocialEventQuestionsRouter, "router is not SocialEventQuestionsRouter")

        let interactor: SocialEventQuestionsInteractor = presenter.interactor as! SocialEventQuestionsInteractor
        XCTAssertNotNil(interactor.output, "output in SocialEventQuestionsInteractor is nil after assembly")
    }

    class SocialEventQuestionsViewControllerMock: SocialEventQuestionsViewController {

    }
}
