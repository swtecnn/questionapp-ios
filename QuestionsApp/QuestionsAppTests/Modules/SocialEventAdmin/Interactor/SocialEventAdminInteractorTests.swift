//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class SocialEventAdminInteractorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockPresenter: ISocialEventAdminInteractorOutput {
        func handRequestObtained(_ handRequest: RaiseHandRequest) {
            
        }
        
        func handRequestError(_ error: Error) {
            
        }
        
        func socialEventFinished() {
            
        }
        
        func questionCountObtained(_ count: Int) {
            
        }
        
        func handRequestCountObtained(_ count: Int) {
            
        }
        
        func socialEventObtained(_ socialEvent: SocialEvent) {
            
        }
        
        func socialEventObtainError(_ error: Error) {
            
        }
    
    }
}
