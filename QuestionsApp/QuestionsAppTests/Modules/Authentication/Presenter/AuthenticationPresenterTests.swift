//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class AuthenticationPresenterTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockInteractor: IAuthenticationInteractor {

        func obtainSocialEvent(with id: String) {
        }

        func checkPassword(_ password: String, code: String, passwordHash: String) -> Bool {

            return true
        }
    }

    class MockRouter: IAuthenticationRouter {
        func openSocialEventViewer(id: String) {
            
        }
        
        func openSocialEventClient(id: String) {
            
        }
        
        func openSocialEventAdmin(id: String) {
            
        }
        

    }

    class MockViewController: IAuthenticationView {
        func dismissAuthentication() {
            
        }
        
        func showProgress() {
        }
        
        func hideProgress() {
        }

        func startScanning() {
        }

        func stopScanning() {
        }

        func showPassword() {
        }

        func dismissPassword() {
        }

        func handleError(_ error: Error) {
        }

        func showIncorrectPasswordError() {
        }
    }
}
