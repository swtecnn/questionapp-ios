//  Copyright © 2019 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class SocialEventClientInteractorTests: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    class MockPresenter: ISocialEventClientInteractorOutput {
        func nameObtained(_ name: String) {
            
        }
        
        func socialEventObtained(_ socialEvent: SocialEvent) {
            
        }
        
        func socialEventObtainError(_ error: Error) {
            
        }
        
        func questionCreated() {
            
        }
        
        func questionCreationError(_ error: Error) {
            
        }
        
        func raiseHandRequestCreated(id: String) {
            
        }
        
        func raiseHandRequestError(_ error: Error) {
            
        }
        
        func raiseHandRequestStatusObtained(_ status: RaiseHandRequestStatus) {
            
        }
        
        func raiseHandRequestCancelled() {

        }
    }
}
