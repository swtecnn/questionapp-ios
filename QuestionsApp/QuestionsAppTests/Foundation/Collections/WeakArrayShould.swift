// Copyright © 2018 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class WeakArrayShould: XCTestCase {
    
    class MyClass {
        let myProperty = "stubProperty"
        let intProperty: Int
        
        init(_ value: Int = 0) {
            intProperty = value
        }
    }
    
    private var array: WeakArray<MyClass>!
    
    override func setUp() {
        super.setUp()
        
        array = WeakArray<MyClass>()
    }
    
    func test_returnValueIfNotReleased() {
        
        addObjectAndRelease() {
            XCTAssert(self.array[0] != nil, "Object should be there")
        }
    }
    
    func test_notReturnValueIfReleased() {
        
        addObjectAndRelease()
        
        XCTAssert(array[0] == nil, "Object shouldn't be there")
    }
    
    func test_iterateThroughRetainedElements() {
        
        let value0 = MyClass(0)
        let value1 = MyClass(1)
        let value2 = MyClass(2)
        let value4 = MyClass(4)
        
        array.append(value0)
        array.append(value1)
        array.append(value2)
        array.append(MyClass(3))
        array.append(value4)
        
        var stringValue = ""
        for value in array {
            
            stringValue.append("\(value.intProperty)")
        }
        
        XCTAssert(stringValue == "0124")
    }
    
    func test_notIterateThroughReleasedElements() {
        
        array.append(MyClass(0))
        array.append(MyClass(1))
        array.append(MyClass(2))

        var stringValue = ""
        for value in array {
            
            stringValue.append("\(value.intProperty)")
        }
        
        XCTAssert(stringValue == "")
    }
    
    func addObjectAndRelease(completion: (() -> Void)? = nil ) {
        let obj = MyClass()
        array.append(obj)
        completion?()
    }
    
}
