// Copyright © 2018 SWTecNN. All rights reserved.

import XCTest
@testable import QuestionsApp

class PathUtilsShould: XCTestCase {

    func test_returnNameOfFile() {
        
        XCTAssert(PathUtils.name(path: "/path/name.ext") == "name.ext")
        XCTAssert(PathUtils.name(path: "///path////name.ext") == "name.ext")
        XCTAssert(PathUtils.name(path: "///name.ext") == "name.ext")
        XCTAssert(PathUtils.name(path: "/nameWithoutExt") == "nameWithoutExt")
    }
    
    func test_returnNameOfFolder() {
        
        XCTAssert(PathUtils.name(path: "/path/folder/") == "folder")
        XCTAssert(PathUtils.name(path: "/path/folder////") == "folder")
        XCTAssert(PathUtils.name(path: "/folder/") == "folder")
        XCTAssert(PathUtils.name(path: "/////folder//////") == "folder")
    }
    
    func test_notReturnNameOfString() {
        
        XCTAssert(PathUtils.name(path: "string") == "string")
    }
    
    func test_returnEmptyNameOfRoot() {
        
        XCTAssert(PathUtils.name(path: "/") == "")
        XCTAssert(PathUtils.name(path: "////") == "")
    }
}
