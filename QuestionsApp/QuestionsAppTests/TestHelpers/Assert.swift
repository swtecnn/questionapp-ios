// Copyright © 2018 SWTecNN. All rights reserved.

import Foundation
import XCTest

@discardableResult
public func assertNotThrows<Result>(file: StaticString = #file, line: UInt = #line, f: () throws -> Result) -> Result? {
    
    var result: Result?
    
    do {
        result = try f()
    }
    catch {
        XCTFail("Error wasn't expected: \(error)", file: file, line: line)
    }
    
    return result
}
